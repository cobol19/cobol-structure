       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BirthDateProgram. 
       AUTHOR. Michael Coughlan. 
       DATA DIVISION . 
       WORKING-STORAGE SECTION. 
       01 BrithDate.
           02 YearOfBirth.
              03 CenturyOB PIC 99.
              03 YearOB PIC 99.
            02 MonthOfBirth PIC 99.
            02 DayOfBirth PIC 99.
       PROCEDURE DIVISION. 
           MOVE 19750215 TO BrithDate
           DISPLAY "Month is = " MonthOfBirth
           DISPLAY "Century of birth is = " CenturyOB
           DISPLAY "Year of birth is = " YearOfBirth
           DISPLAY DayOfBirth "/" MonthOfBirth "/" YearOfBirth
           MOVE ZERO TO YearOfBirth
           DISPLAY "Birth date = " BrithDate.